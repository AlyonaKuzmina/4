Кузьмина Алена Лабораторная 4 "Методы"
Вариант 8

_**Задание 1:**_

Реализовать метод, который производит операцию над числами x и y по формуле (a) и (b), после
чего возвращает наибольшее из полученных значений. Входные параметры метода – два целых
числа. Сделать перегрузку метода для параметров типа double. Сделать перегрузку метода для
параметров типа char.

**a. 5 + 3x**

**b. y/3**

_**Задание 2:**_

Реализовать метод для вычисления значения функции с помощью рекурсивного подхода.

**f(n) = ∑ n k=1 (k−95)/50**

_**Задание 3:**_

Реализовать метод для вычисления значения функции с помощью рекурсивного подхода.

**f(n) = ∏ n k=1 (3k+1)/2**
