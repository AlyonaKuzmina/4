﻿using System;

namespace Кузьмина_Алена_лаб_4__1
{
    class Program
    {
        static void Main(string[] args)
        {
            //a. 5 + 3x
            //b. y / 3

            Console.WriteLine("Введите x и y:");
            int x = Convert.ToInt32(Console.ReadLine());
            int y = Convert.ToInt32(Console.ReadLine());
            int c = ResInt(x, y);
            Console.WriteLine("Ответ: " + c);
                
            Console.WriteLine("Введите x и y:");
            double z = double.Parse(Console.ReadLine());
            double v = double.Parse(Console.ReadLine());
            double s = ResDouble(z, v);
            Console.WriteLine("Ответ: " + s);

            Console.WriteLine("Введите x и y:");
            char d = char.Parse(Console.ReadLine());
            char n = char.Parse(Console.ReadLine());
            int f = ResChar(d, n);
            Console.WriteLine("Ответ: " + f);
        }

        //int

        static int Result1(int x)
        {
            int k = 5 + 3 * x;
            return k;
        }
        static int Result2(int y)
        {
            int k = y / 3;
            return k;
        }
        static int ResInt(int x, int y)
        {
            int a = Result1(x);
            int b = Result2(y);
            int k = Math.Max(a, b);
            return k;
        }
        

        //double

        static double Res1(double v)
        {
            double k = 5 + 3 * v;
            return k;
        }
        static double Res2(double w)
        {
            double k = w / 3;
            return k;
        }
        static double ResDouble(double v, double w)
        {
            double a = Res1(v);
            double b = Res2(w);
            double k = Math.Max(a, b);
            return k;
        }



        //char

        static int R1(char t)
        {
            int m = 5 + 3 * t;
            return m;
        }
        static int R2(char g)
        {
            int e = g / 3;
            return e;
        }


        static int ResChar(char t, char g)
        {
            int c = R1(t);
            int v = R2(g);
            int d = Math.Max(c, v);
            return d;

        }
    }
}
