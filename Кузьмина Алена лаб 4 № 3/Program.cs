﻿using System;

namespace Кузьмина_Алена_лаб_4___3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            double n = Convert.ToDouble(Console.ReadLine());
            double k;

            k = 1;
            Div(k);
            static double Div(double k)
            {
                double result = (3 * k + 1) / 2.0;
                return result;
            }
            static double Proizv(double k) => (k == 1) ? Div(1) : Div(k) * Proizv(k - 1);
            Console.WriteLine($"f({n})=" + Proizv(n));
        }

    }
}
